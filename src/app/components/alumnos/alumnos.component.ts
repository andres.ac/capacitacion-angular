import { Component, OnInit } from '@angular/core';
import { AlumnosService } from 'src/app/services/alumnos.service';
import { Alumno } from 'src/app/models/alumno';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-alumnos',
  templateUrl: './alumnos.component.html',
  styleUrls: ['./alumnos.component.css'],
})
export class AlumnosComponent implements OnInit {
  public alumnos: Alumno[] = [];
  constructor(private alumnosService: AlumnosService, private spinner: NgxSpinnerService) {}

  ngOnInit(): void {
    this.buscarAlumnos();
  }

  buscarAlumnos(): void {
    this.spinner.show();
    this.alumnosService.obtenerAlumnos().subscribe(
      (data) => {
        this.alumnos = data;
        this.spinner.hide();
      },
      (error) => {
        console.log(error);
        this.spinner.hide();
      }
    );
  }
}
